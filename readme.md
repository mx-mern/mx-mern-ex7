Tổ chức database quản lý học viên và lớp học, trong đó:
- Học viên chứa: 
    + email *, 
    + họ tên *, 
    + ngày sinh, 
    + số điện thoại, 
    + lớp * (1 học viên có thể học nhiều lớp)
- Lớp học chứa: 
    + tên lớp *, 
    + khoá học*, 
    + thời gian học*

Viết API để truy vấn học viên theo tên, theo lớp, trả về đầy đủ thông tin học viên và thông tin lớp học
Viết API thêm, sửa, xoá đối với học viên, lớp học.
(* dữ liệu không được bỏ trống)