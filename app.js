const express = require('express');
const bodyparser = require('body-parser');
const app = express();

app.use(bodyparser.json());
var port = 3000;
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/MX7ClassManager', { useNewUrlParser: true });
// Connect database
var db = mongoose.connection;
// Lang nghe xem connect thanh cong hay ko
db.on('error', err => {
    logError(err);
});

// swagger
var swagger = require('./swagger');
swagger(app);

// Lang nghe xem connect thanh cong hay ko, khong nhay vao su kien nao nua
db.once('open', function (err, resp) {
    console.log("Connected");
});

var Schema = mongoose.Schema;

var classSchema = new Schema({
    className: {
        type: String,
        required: 'classNameis required'
    },
    classNumber: {
        type: Number,
        required: 'classNumber is required'
    },
    classTime: {
        type: String,
        required: 'classTime is required'
    }
});

var Class = mongoose.model('Class', classSchema);

var validateEmail = function (email) {
    var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email)
};
var studentSchema = new Schema({
    email: {
        type: String,
        required: 'Email address is required',
        validate: [validateEmail, 'Please fill a valid email address']
    },
    name: {
        type: String,
        required: [true, 'Name is required']
    },
    bithday: {
        type: Date,
    },
    mobile: {
        type: String
    },
    classes: [{ type: Schema.Types.ObjectId, ref: 'Class' }]
});

var Student = mongoose.model('Student', studentSchema);

/**
 * @swagger
 * /class/create:
 *  post:
 *      tags: 
 *          - API TAG
 *      name: Api create class
 *      summary: Api create class
 *      consumes:
 *          - application/json
 *      parameters:
 *          -   name: body
 *              in: body
 *              type: object
 *              required: true
 *              schema:
 *                  type: object
 *                  properties:
 *                      className: 
 *                          type : string
 *                      classNumber: 
 *                          type : string
 *                      classTime: 
 *                          type : string
 *      responses:
 *          -   name: body
 *              in: body
 *              schema: 
 *                  type: object
 *                  properties:
 *                      success:
 *                          type: boolean
 *                      code :
 *                          type: number
 *                      message:
 *                          type: string
 *                      data:
 *                          type: object
 *                  example: {
 *                      "success" : true,
 *                      "code" : 200,
 *                      "message" : "OK",
 *                      "data" : {}
 *                  }   
 */
app.post('/class/create', function (req, res) {
    const newClass = new Class(req.body);
    newClass.save(function (err, data) {
        if (err) {
            res.json({
                message: err.errors,
                success: false
            });
        } else {
            res.json({
                message: "New class created successfully",
                success: true
            });
        }
    })
})

app.post('/class/delete', function (req, res) {
    if (req.body.className) {
        Class.deleteOne({ className: req.body.className }, function (err) {
            if (err) {
                res.json({
                    message: err.errors,
                    success: false
                });
            } else {
                res.json({
                    message: `Class ${req.body.className} was removed`,
                    success: true
                });
            }
        });
        // Class.findOne({className: req.body.className}).remove(function(err) {
        // })
    }
})

app.post('/student/create', function (req, res) {
    const newStudent = new Student(req.body);
    newStudent.save(function (err, data) {
        if (err) {
            res.json({
                message: err.errors,
                success: false
            });
        } else {
            res.json({
                message: "New Student created successfully",
                new_student: newStudent.populate('classes'),
                success: true
            });
        }
    })
})

app.post('/student/delete', function (req, res) {
    if (req.body.name) {
        Student.findOne({ name: req.body.name }).remove(function (err) {
            if (err) {
                res.json({
                    message: err.errors,
                    success: false
                });
            } else {
                res.json({
                    message: `Student ${req.body.name} was removed`,
                    success: true
                });
            }
        })
    }
})


app.post('/student/edit', function (req, res) {
    if (req.body.name) {
        let student = Student.findOne({ name: req.body.name }).exec(function (err, student) {
            if (err) {
                res.json({
                    message: err.errors,
                    success: false
                });

            } else if (student) {
                console.log("find student" + JSON.stringify(student));
                if (req.body.email) {
                    student.email = req.body.email;
                }
                if (req.body.birthday) {
                    student.birthday = req.body.birthday;
                }
                if (req.body.newName) {
                    student.name = req.body.newName;
                }
                if (req.body.mobile) {
                    student.mobile = req.body.mobile;
                }
                if (req.body.classes) {
                    student.classes = req.body.classes;
                }
                student.save(function (err, data) {
                    if (err) {
                        res.json({
                            message: err.errors,
                            success: false
                        });
                    } else {
                        res.json({
                            message: "Student updated successfully",
                            update_student: student.populate('classes'),
                            success: true
                        });
                    }
                })
            } else {
                res.json({
                    message: `Student not found`,
                    success: false
                });
            }
        });
    } else {
        res.json({
            message: `Student name is empty`,
            success: false
        });
    }
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))